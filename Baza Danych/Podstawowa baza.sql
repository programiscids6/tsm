drop table Nalezy
Drop table Wyswietlone
drop table Wiadomosci
drop table Reklamy
drop table Uzytkownicy
drop table Pokoje
drop trigger ilosc_w_pokoju
drop trigger ilosc_w_pokoju2
drop trigger  ilosc_wiadomosci
drop trigger  ilosc_wiadomosci2

ALTER DATABASE [Mieszkanie studenckie] COLLATE Polish_CI_AS



Create Table Uzytkownicy(
Id_uzytkownika int Primary Key Identity(1,1),
Email nvarchar(80),
Login nvarchar(30),
Haslo  nvarchar(30));


Create Table Pokoje(
Id_pokoju int Primary Key Identity(1,1),
Nazwa nvarchar(40),
Opis nvarchar(250),
Ilosc_uzytkownikow int,
Tag nvarchar(255),
Miejsce nvarchar(30),
Ilosc_postow int,
Wlasciciel int);


-- Nie robie klucza g��wnego z dw�ch obcych �eby problemu nie by�o
Create table Nalezy(
Id_nalezy int Primary Key Identity(1,1),
Id_uzytkownika int  references Uzytkownicy(Id_uzytkownika),
Id_pokoju int  references Pokoje(Id_pokoju));

Create table Wiadomosci(
Id_ogloszenia int Primary key Identity (1,1),
Id_pokoju int references Pokoje(Id_pokoju),
Id_uzytkownika int references Uzytkownicy(Id_uzytkownika),
Tytul nvarchar(30),
Tresc nvarchar(250),
data date);


Create table Reklamy(
Id_reklamy int Primary key Identity(1,1),
Zdjecie varchar(MAX),
Data date,
Miejsce nvarchar(30),
Docelowa_ilosc_wys int,
aktualna_ilosc_wys int,
budzet_poczatkowy float,
aktualny_budzet float,
haslo nvarchar(30),
Tag nvarchar(255));

Create table Wyswietlone(
Id_wyswietlenia int Primary key Identity (1,1),
Id_reklamy int  references Reklamy(Id_reklamy),
Id_uzytkownika int  references Uzytkownicy(Id_uzytkownika),
data date);

go
Create trigger ilosc_w_pokoju 
on Nalezy
after insert
as 
begin 
update Pokoje set Ilosc_uzytkownikow = Ilosc_uzytkownikow +1
from Pokoje p,inserted i
where  i.Id_pokoju = p.Id_pokoju
end

go
Create trigger ilosc_w_pokoju2 
on Nalezy
after delete
as 
begin 
update Pokoje set Ilosc_uzytkownikow = Ilosc_uzytkownikow  - 1
from Pokoje p,deleted d
where  d.Id_pokoju = p.Id_pokoju
end

go
Create trigger ilosc_wiadomosci
on Wiadomosci
after insert
as 
begin 
update Pokoje set Ilosc_postow = Ilosc_postow +1
from Pokoje p,inserted i
where  i.Id_pokoju = p.Id_pokoju
end

go
Create trigger ilosc_wiadomosci2
on Wiadomosci
after delete
as 
begin 
update Pokoje set Ilosc_postow = Ilosc_postow -1
from Pokoje p,inserted i
where  i.Id_pokoju = p.Id_pokoju
end
