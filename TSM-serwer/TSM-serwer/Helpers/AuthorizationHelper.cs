﻿using TSM_serwer.Models.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace TSM_serwer.Helpers
{
    /// <summary>
    /// Klasa pomocnicza do celów autoryzacji użytkowników.
    /// </summary>
    public static class AuthorizationHelper
    {
        /// <summary>
        /// Pobieranie nazwy użytkownika z nagłówka HTTP
        /// </summary>
        /// <param name="header">Nagłówek HTTP</param>
        /// <returns>Nazwa użytkownika (string).</returns>
        public static string getUserName(HttpRequestHeaders header)
        {
            // Pobieranie danych autoryzacji z nagłówka HTTP
            AuthenticationHeaderValue authorization = header.Authorization;
            // Konwersja danych do postaci jawnej
            byte[] bytes = Convert.FromBase64String(authorization.Parameter);
            string plainCredentials = Encoding.ASCII.GetString(bytes);

            // Pobieranie nazwy użytkownika
            int colonIndex = plainCredentials.IndexOf(":");
            string userName = plainCredentials.Substring(0, colonIndex);
            return userName;
        }//getUserName()

        public static bool isUserInRoom(HttpRequestHeaders header, int roomId)
        {
            int userId = getUserId(header);
            try
            {
                using (var db = new TSMEntities())
                {
                    db.Nalezy.First(n => n.Id_pokoju == roomId && n.Id_uzytkownika == userId);
                }//using
                return true;
            }//try
            catch(InvalidOperationException)
            {
                return false;
            }//catch
        }

        /// <summary>
        /// Pobieranie nazwy ID użytkownika z bazy na podstawie danych z nagłówka HTTP
        /// </summary>
        /// <param name="header">Nagłówek HTTP</param>
        /// <returns>Id użytkownika (int).</returns>
        public static int getUserId(HttpRequestHeaders header)
        {
            string userName = getUserName(header);
            using (var db = new TSMEntities())
            {
                var query = from user in db.Uzytkownicy
                            where user.Login == userName
                            select user.Id_uzytkownika;
                return query.First();
            }// using
        }//getUserId()

        /// <summary>
        /// Czy użytkownik może modyfikować dane mieszkanie
        /// </summary>
        /// <param name="header">Nagłowek HTTP</param>
        /// <param name="flatId">ID mieszkania</param>
        /// <returns>true - tak, false - nie</returns>
        public static bool isUserAuthorized(HttpRequestHeaders header, int flatId)
        {
            int userId = getUserId(header);
            int? userFlatId = getUserFlatId(userId);
            if (userFlatId != null)
            {
                if (userFlatId == flatId)
                {
                    return true;
                }
            }//if
            return false;
        }//isUserAuthorized()

        /// <summary>
        /// Pobieranie ID mieszkania przypisanego do danego użytkownika
        /// </summary>
        /// <param name="header">Nagłówek HTTP</param>
        /// <returns>ID mieszkania (int?)</returns>
        public static int? getUserFlatId(HttpRequestHeaders header)
        {
            int userdId = getUserId(header);
            return getUserFlatId(userdId);
        }//getUserFlatId()

        /// <summary>
        /// Pobieranie ID mieszkania przypisanego do danego użytkownika
        /// </summary>
        /// <param name="userId">ID użytkownika</param>
        /// <returns>ID mieszkania (int?)</returns>
        public static int? getUserFlatId(int userId)
        {
            using (var db = new TSMEntities())
            {
                return 0;
                //return db.Uzytkownicy.First(u => u.Id_uzytkownika == userId).Id_mieszkania;
            }//using
        }//getUserFlatId()
    }//AuthorizationHelper
}