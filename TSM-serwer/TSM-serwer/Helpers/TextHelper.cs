﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace TSM_serwer.Helpers
{
    public static class TextHelper
    {
        /// <summary>
        /// Usuwanie białych znaków z łańcucha.
        /// </summary>
        /// <param name="str">Wejściowy łańcuch znaków</param>
        /// <returns>Wejściowy łańcuch bez białych znaków</returns>
        public static string removeWhiteSpaces(string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.Length; i++)
            {
                if (!char.IsWhiteSpace(str[i]))
                {
                    sb.Append(str[i]);
                }//if
            }//for

            return sb.ToString();
        }//removeWhiteSpaces()
    }
}