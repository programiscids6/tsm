﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq;
using System;
using System.Threading.Tasks;
using TSM_serwer.Filters;
using TSM_serwer.Models.DB;
using TSM_serwer.Helpers;
using TSM_serwer.Models;

namespace MieszkanieStudenckieSerwer.Controllers
{
    /// <summary>
    /// Ogłoszenia
    /// 
    /// api/publication
    /// </summary>
    [IdentityBasicAuthentication]
    [Authorize]
    public class PublicationController : ApiController
    {
        /// <summary>
        /// GET: api/publication/{id}
        /// 
        /// Lista ogłoszeń z danego pokoju.
        /// </summary>
        /// <returns>Lista ogłoszeń</returns>
        public HttpResponseMessage Get(int id)
        {
            try
            {
                using (var db = new TSMEntities())
                {
                    var query = (from publication in db.Wiadomosci
                                 join users in db.Uzytkownicy on publication.Id_uzytkownika equals users.Id_uzytkownika
                                 where id == publication.Id_pokoju
                                 select new
                                 {
                                     publicationId = publication.Id_ogloszenia,
                                     flatId = publication.Id_pokoju,
                                     userName = users.Login,
                                     title = publication.Tytul,
                                     description = publication.Tresc
                                 }).ToList();

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        Publications = query
                    });
                }//using
            }//try
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " " + ex.ToString());
            }//catch
        }//Get()

        /// <summary>
        /// POST: api/publication
        /// 
        /// Dodawanie ogłoszeń
        /// </summary>
        /// <param name="publication">Ogłoszenie do dodania</param>
        /// <returns>ID dodanego ogłoszenia lub komunikat o błędzie</returns>
        public HttpResponseMessage Post([FromBody]PublicationModel publication)
        {
            int userId = AuthorizationHelper.getUserId(Request.Headers);

            if (!AuthorizationHelper.isUserInRoom(Request.Headers, publication.roomId))
                return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " Użytkownik nie jest przypisany do tego pokoju.");

            try
            {
                using (var db = new TSMEntities())
                {
                    Wiadomosci publicationToAdd = new Wiadomosci
                    {
                        Id_ogloszenia = publication.publicationId,
                        Id_pokoju = publication.roomId,
                        Id_uzytkownika = userId,
                        data = DateTime.Now,
                        Tresc = publication.description,
                        Tytul = publication.title
                    };

                    db.Wiadomosci.Add(publicationToAdd);
                    db.SaveChanges();

                    return Request.CreateResponse(HttpStatusCode.OK, publicationToAdd.Id_ogloszenia);
                }//using               
            }//try
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " " + ex.ToString());
            }//catch
        }//Post()

        /// <summary>
        /// PUT: api/publication
        /// 
        /// Aktualizacja istniejącego ogłoszenia.
        /// </summary>
        /// <param name="publication">Uaktualnione ogłoszenie</param>
        /// <returns>Success lub błąd</returns>
        public async Task<HttpResponseMessage> Put([FromBody]PublicationModel publication)
        {
            return await Task<HttpResponseMessage>.Factory.StartNew(() =>
            {
                int userId = AuthorizationHelper.getUserId(Request.Headers);
                if (!AuthorizationHelper.isUserInRoom(Request.Headers, publication.roomId))
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " Użytkownik nie jest przypisany do tego pokoju.");

                try
                {
                    using (var db = new TSMEntities())
                    {
                        // pobranie ogłoszenia
                        var updatedPub = db.Wiadomosci.First(p => p.Id_ogloszenia == publication.publicationId &&
                            p.Id_uzytkownika == userId && p.Id_pokoju == publication.roomId);

                        // aktualizacja
                        updatedPub.Tresc = publication.description;
                        updatedPub.Tytul = publication.title;

                        // zapisanie zmian w bazie
                        db.Wiadomosci.Attach(updatedPub);
                        var entry = db.Entry(updatedPub);
                        entry.Property(e => e.Tresc).IsModified = true;
                        entry.Property(e => e.Tytul).IsModified = true;
                        db.SaveChanges();
                    }//using
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.SUCCESS);
                }//try
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " " + ex.ToString());
                }//catch
            });
        }//Put()

        /// <summary>
        /// DELETE: api/publication
        /// 
        /// Usuwanie ogłoszenia.
        /// </summary>
        /// <param name="publication">Ogłoszenie do usunięcia (wystarczy id)</param>
        /// <returns>Success lub błąd</returns>
        public async Task<HttpResponseMessage> Delete([FromBody]PublicationModel publication)
        {
            return await Task<HttpResponseMessage>.Factory.StartNew(() =>
            {
                int userId = AuthorizationHelper.getUserId(Request.Headers);

                if (!AuthorizationHelper.isUserInRoom(Request.Headers, publication.roomId))
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " Użytkownik nie jest przypisany do tego pokoju.");
                try
                {
                    using (var db = new TSMEntities())
                    {
                        // pobranie ogłoszenia
                        var pubToDel = db.Wiadomosci.First(p => p.Id_ogloszenia == publication.publicationId &&
                            p.Id_uzytkownika == userId);

                        // usunięcie ogłoszenia
                        db.Wiadomosci.Remove(pubToDel);
                        db.SaveChanges();
                    }//using
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.SUCCESS);
                }//try
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " " + ex.ToString());
                }//catch
            });
        }//Delete()
    }
}

