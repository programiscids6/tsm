﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSM_serwer.Filters;
using TSM_serwer.Helpers;
using TSM_serwer.Models;
using TSM_serwer.Models.DB;

namespace TSM_serwer.Controllers
{
    /// <summary>
    /// Kontroler konta. Umożliwia zarządzanie kontem (logowanie, rejestrację i modyfikację danych)
    /// </summary>
    public class AccountController : ApiController
    {
        /// <summary>
        /// Logowanie
        /// </summary>
        /// <returns>Łańcuch znaków: 
        ///  - Success w przypadku udanego logowania
        ///  - informację o błędzie gdy dane są niepoprawne</returns>
        [IdentityBasicAuthentication]
        [Authorize]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, Responses.SUCCESS);
        }//Get()

        /// <summary>
        /// Rejestracja
        /// 
        /// TODO: Sprawdzanie czy email'a nie ma już w bazie
        /// 
        /// </summary>
        /// <param name="user">Obiekt klasy pomocniczej UserModel zawierający dane o użytkowniku</param>
        /// <returns></returns>
        [Route("api/account/register")]
        [HttpPost]
        public HttpResponseMessage Register(UserModel user)
        {
            // insert
            using (var db = new TSMEntities())
            {
                string existStrUser = null;
                var users = db.Set<Uzytkownicy>();
                try
                {
                    string checkUniq = (from existUser in users
                                        where existUser.Login == user.username
                                        select existUser).First().ToString();
                    existStrUser = checkUniq;
                }
                catch (Exception)
                {
                    existStrUser = null;
                }//catch

                if (existStrUser == null)
                {
                    users.Add(new Uzytkownicy { Login = user.username, Haslo = user.password, Email = user.email });

                    db.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.SUCCESS);
                }//if
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, Responses.LOGIN_IN_USE);
                }//else
            }//using
        }//Register()

        /// <summary>
        /// Dodawanie użytkownika do mieszkania
        /// </summary>
        /// <param name="room">Dane mieszkania do którego należy dodać użytkownika w formacie JSON:
        /// {
        ///     "id":id
        /// } 
        /// </param>
        /// <returns>Success jeśli się powiodło lub komunikat o błędzie.</returns>
        [IdentityBasicAuthentication]
        [Authorize]
        [HttpPut]
        [Route("api/account/addToRoom")]
        public HttpResponseMessage addToRoom([FromBody]RoomModel room)
        {
            try
            {
                int userId = AuthorizationHelper.getUserId(Request.Headers);
                using (var db = new TSMEntities())
                {
                    try
                    {
                        // sprawdzam czy takie powiązanie jest już bazie
                        db.Nalezy.First(e => e.Id_uzytkownika == userId && e.Id_pokoju == room.id);
                        return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL + " użytkownik już należy do tego pokoju.");
                    }//try
                    catch (InvalidOperationException)
                    {
                        Nalezy link = new Nalezy { Id_pokoju = room.id, Id_uzytkownika = userId };
                        db.Nalezy.Add(link);
                        db.SaveChanges();
                    }//catch
                }//using
            }//try
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.ToString());
            }//catch

            return Request.CreateResponse(HttpStatusCode.OK, Responses.SUCCESS);
        }//Put()
    }//AccountController
}
