﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TSM_serwer.Helpers;
using TSM_serwer.Models;
using TSM_serwer.Models.DB;

namespace TSM_serwer.Controllers
{
    /// <summary>
    /// Kontroler pokojów
    /// 
    /// GET - lista pokojów
    /// POST - dodanie nowego pokoju
    /// PUT - aktualizacja danych istniejącego pokoju
    /// </summary>
    public class RoomsController : ApiController
    {
        /// <summary>
        /// Wysyłanie listy pokojów
        /// </summary>
        /// 
        /// <returns>Lista pokojów (JSON)</returns>
        public HttpResponseMessage Get()
        {
            using (var db = new TSMEntities())
            {
                var query = (from room in db.Pokoje
                             select new
                             {
                                 room.Id_pokoju,
                                 room.Nazwa,
                                 room.Opis,
                                 room.Tag,
                                 room.Miejsce
                             }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Rooms = query
                });
            }//using
        }//Get()

        /// <summary>
        /// Dodawanie nowego pokoju
        /// </summary>
        /// 
        /// <param name="roomModel"> Pokój w formacie JSON:
        /// {
        ///     "name": nazwa(string),
        ///     "description": opis(string),
        ///     "tags": tagi(string, oddzielone przecinkami)
        /// }
        /// </param>
        /// 
        /// <returns>Odpowiedź HTTP z ID utworzonego pokoju</returns>
        [HttpPost]
        public HttpResponseMessage Post(RoomModel roomModel)
        {
            int userId = AuthorizationHelper.getUserId(Request.Headers);
            string strTags = removeWhiteSpaces(roomModel.tags);
            using (var db = new TSMEntities())
            {
                var rooms = db.Set<Pokoje>();
                Pokoje room = new Pokoje { Nazwa = roomModel.name, Opis = roomModel.description, Tag = strTags, Wlasciciel = userId, Miejsce = roomModel.place };
                rooms.Add(room);
                db.SaveChanges();

                Nalezy link = new Nalezy { Id_pokoju = room.Id_pokoju, Id_uzytkownika = userId };
                db.Nalezy.Add(link);
                db.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK, room.Id_pokoju);
            }//using
            //return Request.CreateResponse(HttpStatusCode.OK, Responses.FAIL);
        }//Post()

        [Route("api/rooms/search")]
        [HttpGet]
        public HttpResponseMessage search(string tag)
        {
            List<dynamic> roomList = new List<dynamic>();
            using (var db = new TSMEntities())
            {
                var rooms = db.Set<Pokoje>();
                foreach(var room in rooms)
                {
                    if(room.Tag.Contains(tag))
                    {
                        roomList.Add(new
                        {
                            room.Id_pokoju,
                            room.Nazwa,
                            room.Opis,
                            room.Tag,
                            room.Miejsce
                        });
                    }//if
                }//foreach
            }//using

            return Request.CreateResponse(HttpStatusCode.OK, new
            {
                Rooms = roomList
            });
        }//search()

        /// <summary>
        /// Usuwanie białych znaków z łańcucha.
        /// </summary>
        /// <param name="str">Wejściowy łańcuch znaków</param>
        /// <returns>Wejściowy łańcuch bez białych znaków</returns>
        private static string removeWhiteSpaces(string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.Length; i++)
            {
                if (!char.IsWhiteSpace(str[i]))
                {
                    sb.Append(str[i]);
                }//if
            }//for

            return sb.ToString();
        }//removeWhiteSpaces()
    }//RoomsController
}
