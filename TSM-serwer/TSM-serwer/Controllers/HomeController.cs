﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TSM_serwer.Helpers;
using TSM_serwer.Models;
using TSM_serwer.Models.DB;

namespace TSM_serwer.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        [HttpPost]
        public ActionResult addAd(AdModel adModel)
        {
            if(ModelState.IsValid)
            {
                adModel.Tag = TextHelper.removeWhiteSpaces(adModel.Tag);
                using (MemoryStream ms = new MemoryStream())
                {
                    adModel.obraz.InputStream.CopyTo(ms);
                    byte[] imageBytes = ms.GetBuffer();
                    string base64String = Convert.ToBase64String(imageBytes);

                    Reklamy ad = new Reklamy()
                    {
                        Data = DateTime.Now,
                        budzet_poczatkowy = adModel.budzet,
                        Miejsce = adModel.Miejsce,
                        Tag = adModel.Tag,
                        Docelowa_ilosc_wys = adModel.doceloweWyswietlenia,
                        aktualny_budzet = adModel.budzet,
                        aktualna_ilosc_wys = 0,
                        haslo = adModel.haslo,
                        Zdjecie = base64String
                    };

                    using (var db = new TSMEntities())
                    {
                        db.Reklamy.Add(ad);
                        db.SaveChanges();
                    }//using

                    ViewBag.Id = ad.Id_reklamy;
                    ViewBag.Tags = ad.Tag;
                    ViewBag.Place = ad.Miejsce;
                    ViewBag.doceloweWyswietlenia = ad.Docelowa_ilosc_wys;

                    return View("AdInfo");
                }//using
            }
            return View("Index", adModel);
        }

        public ActionResult GetAd()
        {
            return View("GetAd");
        }

        [HttpPost]
        public ActionResult GetAdState(AdAuthModel adAuth)
        {
            if (ModelState.IsValid)
            {
                using (var db = new TSMEntities())
                {
                    var ad = db.Reklamy.First(f => f.Id_reklamy == adAuth.id && f.haslo == adAuth.haslo);
                    try
                    {
                        ViewBag.Id = ad.Id_reklamy;
                        ViewBag.Tags = ad.Tag;
                        ViewBag.Place = ad.Miejsce;
                        ViewBag.doceloweWyswietlenia = ad.Docelowa_ilosc_wys;
                        ViewBag.aktualneWyswietlenia = ad.aktualna_ilosc_wys;

                        return View("AdInfo");
                    }//try
                    catch (InvalidOperationException)
                    {
                        //nic nie rób
                    }//catch
                }//using
            }
            return View("GetAd", adAuth);
        }
    }
}
