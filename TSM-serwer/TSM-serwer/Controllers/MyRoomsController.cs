﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using TSM_serwer.Filters;
using TSM_serwer.Helpers;
using TSM_serwer.Models;
using TSM_serwer.Models.DB;

namespace TSM_serwer.Controllers
{
    /// <summary>
    /// Kontroler Moje mieszkanie
    /// GET - dane o moim mieszkaniu
    /// </summary>
    [IdentityBasicAuthentication]
    [Authorize]
    public class MyRoomsController : ApiController
    {
        /// <summary>
        /// GET: api/MyRooms
        /// 
        /// Pobieranie informacji o pokojach do których należy użytkownik
        /// </summary>
        /// <returns>Lista pokojów</returns>
        public HttpResponseMessage Get()
        {
            int userId = AuthorizationHelper.getUserId(Request.Headers);
            List<dynamic> myRooms = new List<dynamic>();
            using (var db = new TSMEntities())
            {
                var linkList = db.Nalezy.Where(l => l.Id_uzytkownika == userId).ToList();
                foreach(var link in linkList)
                {
                    Pokoje room = db.Pokoje.First(r => r.Id_pokoju == link.Id_pokoju);
                    myRooms.Add(new
                    {
                        room.Id_pokoju,
                        room.Nazwa,
                        room.Opis,
                        room.Tag,
                        room.Miejsce
                    });
                }//foreach
            }//using

            return Request.CreateResponse(HttpStatusCode.OK, new
            {
                MyRooms = myRooms
            });
        }//Get()

        /// <summary>
        /// GET: api/MyRooms/Members?id={id}
        /// 
        /// Pobieranie listy użytkowników z danego pokoju
        /// </summary>
        /// <param name="id">ID pokoju</param>
        /// <returns></returns>
        [Route("api/MyRooms/Members")]
        [HttpGet]
        public HttpResponseMessage Members(int id)
        {
            List<dynamic> memebers = new List<dynamic>();
            using (var db = new TSMEntities())
            {
                var linkList = db.Nalezy.Where(l => l.Id_pokoju == id).ToList();
                foreach(var link in linkList)
                {
                    Uzytkownicy user = db.Uzytkownicy.First(u => u.Id_uzytkownika == link.Id_uzytkownika);
                    memebers.Add(new
                    {
                        user.Login
                    });
                }//foreach
            }//using

            return Request.CreateResponse(HttpStatusCode.OK, new
            {
                Members = memebers
            });
        }//Members()

        /// <summary>
        /// Aktualizacja nazwy i opisu pokoju
        /// </summary>
        /// <param name="roomModel">JSON: 
        ///     {
        ///         "id": id(int),
        ///         "name": nazwa(string),
        ///         "description": opis(string),
        ///         "tags": tagi(string)
        ///     }
        /// </param>
        /// <returns>Zaktualizowany obiekt pokoju</returns>
        [HttpPut]
        public HttpResponseMessage Put(RoomModel roomModel)
        {
            int userId = AuthorizationHelper.getUserId(Request.Headers);
            roomModel.tags = removeWhiteSpaces(roomModel.tags);

            using (var db = new TSMEntities())
            {
                // aktualizacja krotki
                var updatedRoom = db.Pokoje.First(u => u.Id_pokoju == roomModel.id);
                if (updatedRoom.Wlasciciel == userId)
                {
                    string strTags = removeWhiteSpaces(roomModel.tags);
                    updatedRoom.Nazwa = roomModel.name;
                    updatedRoom.Opis = roomModel.description;
                    updatedRoom.Tag = roomModel.tags;
                    updatedRoom.Miejsce = roomModel.place;

                    db.Pokoje.Attach(updatedRoom);
                    var entry = db.Entry(updatedRoom);
                    entry.Property(e => e.Nazwa).IsModified = true;
                    entry.Property(e => e.Opis).IsModified = true;
                    entry.Property(e => e.Tag).IsModified = true;
                    entry.Property(e => e.Miejsce).IsModified = true;
                    db.SaveChanges();

                    return Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        updatedRoom.Id_pokoju,
                        updatedRoom.Nazwa,
                        updatedRoom.Opis,
                        updatedRoom.Tag,
                        updatedRoom.Miejsce
                    });
                }//if

                return Request.CreateResponse(HttpStatusCode.OK, Responses.ACCESS_DENIED);
            }//using
        }//Put()

        /// <summary>
        /// Usuwanie białych znaków z łańcucha.
        /// </summary>
        /// <param name="str">Wejściowy łańcuch znaków</param>
        /// <returns>Wejściowy łańcuch bez białych znaków</returns>
        private static string removeWhiteSpaces(string str)
        {
            if (string.IsNullOrEmpty(str))
                return "";

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.Length; i++)
            {
                if (!char.IsWhiteSpace(str[i]))
                {
                    sb.Append(str[i]);
                }//if
            }//for

            return sb.ToString();
        }//removeWhiteSpaces()
    }//MyRoomsController
}
