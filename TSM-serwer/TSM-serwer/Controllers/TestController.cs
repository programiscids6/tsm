﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TSM_serwer.Models.DB;

namespace TSM_serwer.Controllers
{
    /// <summary>
    /// Kontroler testowy
    /// </summary>
    public class testController : ApiController
    {
        /// <summary>
        /// HTTP GET api/test
        /// </summary>
        /// <returns>tabela Users z bazy danych jako JSON</returns>
        public HttpResponseMessage Get()
        {
            using (var db = new TSMEntities())
            {
                var query = (from users in db.Uzytkownicy
                             select new
                             {
                                 users.Id_uzytkownika,
                                 users.Login,
                                 users.Haslo,
                                 users.Email
                             }).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Users = query
                });
            }// using
        }//Get()
    }//testController
}
