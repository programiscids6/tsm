﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSM_serwer.Models
{
    /// <summary>
    /// Klasa pomocnicza, model pokoju.
    /// </summary>
    public class RoomModel
    {
        public int id;
        public string name;
        public string description;
        public string tags { get; set; }
        public int ownerId { get; set; }
        public string place { get; set; }
    }
}