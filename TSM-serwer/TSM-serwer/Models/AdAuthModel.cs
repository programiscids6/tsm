﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TSM_serwer.Models
{
    public class AdAuthModel
    {
        [Required]
        [Display(Name = "Identyfikator")]
        public int id { get; set; }
        [Required]
        [Display(Name = "hasło")]
        public string haslo { get; set; }
    }
}