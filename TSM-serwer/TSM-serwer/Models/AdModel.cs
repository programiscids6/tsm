﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TSM_serwer.Models
{
    public class AdModel
    {
        public int id { get; set; }

        [Required]
        [Display(Name = "tagi")]
        public string Tag { get; set; }

        [Required]
        [Display(Name = "miejsce")]
        public string Miejsce { get; set; }

        [Required]
        [Display(Name = "docelowa ilość wyświetleń")]
        public int doceloweWyswietlenia { get; set; }

        [Required]
        [Display(Name = "budżet reklamy")]
        public float budzet { get; set; }

        [Required]
        [Display(Name = "hasło")]
        public string haslo { get; set; }

        [Compare("haslo")]
        [Display(Name = "Potwierdź hasło")]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Obraz do wyświetlenia")]
        public HttpPostedFileBase obraz { get; set; }
    }
}