﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TSM_serwer.Models
{
    public class PublicationModel
    {
        public int publicationId { get; set; }
        public int roomId { get; set; }
        public int userId { get; set; }
        public string title { get; set; }
        public string description { get; set; }
    }
}