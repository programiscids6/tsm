# README #

Serwer

### Podstawowe api: ###

* api/test - testowy kontroler
* api/account - obsługa kont użytkowników (logowanie, rejestracja)
* api/rooms - obsługa pokojów (lista, dodawanie nowych)
* api/myrooms - moje pokoje (lista, aktualizacja, lista członków)
* api/publication - ogłoszenia z danego pokoju
* api/ads - reklamy

### api/test: ###

* GET - zwraca listę użytkowników

### api/account: ###

* GET - logowanie, zwraca: "Success" w przypadku zalogowania
* POST - dodawanie nowego użytkownika, przyjmuje JSON:
	{
		"username":"login",
		"password":"haslo",
		"email":"email"
	}
* PUT: api/account/addToRoom - dołączenie do pokoju, przyjmuje:
	{
        "id":id(int)
    }

### api/rooms: ###

* GET - lista pokojów
* POST - dodanie nowego pokoju, przyjmuje JSON:
	{
        "name":"nazwa",
        "description":"opis",
		"tags": tagi(string, oddzielone przecinkami),
		"place": miejsce(string)
	}
	zwraca: ID pokoju
* GET api/rooms/search?tag={szukany_wyraz}
	zwraca listę pasujących pokojów
	
### api/myrooms ###

* GET - lista pokojów użytkownika
* PUT - aktualizacja nazwy i opisu pokoju, przyjmuje JSON:
	{
        "id":id,
        "name":"nazwa",
        "description":"opis",
		"tags": tagi(string, oddzielone przecinkami),
		"place": miejsce(string)
	}
	zwraca: zaktualizowane dane pokoju lub błąd
* GET api/myrooms/members?id={id} - lista użytkowników w danym pokoju
	{
		"Login": nazwa_uzytkownika(string)
	}
	

### api/publication ###

* GET api/publication/{idPokoju} - lista ogłoszeń
* POST api/publication - dodawanie ogłoszenia, przyjmuje:
	{
        "roomId":idPokoju(int),
        "title":tytuł(string),
        "description":opis(string)
	}
	zwraca ID ogłoszenia
* PUT api/publication - aktualizacja ogłoszenia
	{
      "publicationId": idOgłoszenia(int),
      "roomId": idPokoju(int),
      "userId": idUzytkownika(int),
      "title": tytuł(string),
      "description": opis(string)
    }
* DELETE api/publication - usuwanie ogłoszenia
	{
		"publicationId": idOgłoszenia(int),
		"roomId": 1
	}
	

### api/ads ###

* GET - zwraca reklamę jako string,
	przyjmuje: idPokoju(int) - GET api/ads/{idPokoju}
	zwraca: reklama(string)