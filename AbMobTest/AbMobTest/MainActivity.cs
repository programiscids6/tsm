﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Gms.Ads;
using admobDemo.AndroidPhone.ad;

namespace AbMobTest
{
    [Activity(Label = "AbMobTest", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        int count = 1;

        // tutaj zmienna z banerem
        AdView _bannerad;
        

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button button = FindViewById<Button>(Resource.Id.MyButton);

            button.Click += delegate { button.Text = string.Format("{0} clicks!", count++); };

            // tutaj całość dodana z przykładu. Powinny się wyświetlić dwie reklamy.

            //----------------------------------------------banner add stuff
            _bannerad = AdWrapper.ConstructStandardBanner(this, AdSize.SmartBanner, "ca-app-pub-6639608776412151/9636308429");
            var listener = new admobDemo.adlistener();
            
            listener.AdLoaded += () => { };
            _bannerad.AdListener = listener;
            _bannerad.CustomBuild();
            var layout = FindViewById<LinearLayout>(Resource.Id.mainlayout);
            layout.AddView(_bannerad);
            //-------------------------------------------------------------



            //  ta reklama chyba nam się nie przyda, ale zostawiam jako przykład

            //-------------------------------------------------InterstitialAd stuff
            var FinalAd = AdWrapper.ConstructFullPageAdd(this, "ca-app-pub-6639608776412151/2993052020");
            var intlistener = new admobDemo.adlistener();
            intlistener.AdLoaded += () => { if (FinalAd.IsLoaded) FinalAd.Show(); };
            FinalAd.AdListener = intlistener;
            FinalAd.CustomBuild();
            //-------------------------------------------------------------

        }

        protected override void OnResume()
        {
            if (_bannerad != null) _bannerad.Resume();
            base.OnResume();
        }
        protected override void OnPause()
        {
            if (_bannerad != null) _bannerad.Pause();
            base.OnPause();
        }
    }
}

