-- Baza zosta�a stworzona "na szybko" na potrzeby postawienia podstawowego serwera.
-- Zawiera ona tylko podstawowe tabele do logowania i listy pokoj�w.
-- UWAGA! Baz� trzeba jeszcze raz porz�dznie zaprojektowa� na nowo lub rozbudowa� to co jest.

-- ----------------
-- czyszczenie bazy
-- ----------------
IF OBJECT_ID('Users') IS NOT NULL
DROP TABLE Users;
IF OBJECT_ID('Rooms') IS NOT NULL
DROP TABLE Rooms;

use [TSM]

-- zmiana kodowania na obs�uguj�ce j�zyk polski 
ALTER DATABASE [TSM] COLLATE Polish_CI_AS
GO

-- ---------------
-- tworzenie tabel
-- ---------------

-- U�ytkownicy
Create Table Users(
UserID int Primary Key Identity(1,1),
userName nvarchar(30),
userPassword  nvarchar(30),
email nvarchar(30));

-- Pokoje
Create Table Rooms(
RoomID int Primary Key Identity(1,1),
roomName nvarchar(30),
roomDescription nvarchar(255));

-- -----------------------
-- Wype�nianie bazy danymi
-- -----------------------

-- U�ytkownicy
INSERT INTO Users
VALUES ('Bogdan', 'haslo', 'bogdan@email.pl');
INSERT INTO Users
VALUES ('Stefan', 'haslo', 'Stefan@email.pl');
INSERT INTO Users
VALUES ('Marek', 'haslo', 'Marek@email.pl');

-- Pokoje
INSERT INTO Rooms
VALUES ('Filmy', 'Pok�j dla zainteresowanych filmami.');
INSERT INTO Rooms
VALUES ('Motoryzacja', 'Pok�j dla zainteresowanych motoryzacj�.');
INSERT INTO Rooms
VALUES ('Muzyka', 'Pok�j dla zainteresowanych muzyk�.');
INSERT INTO Rooms
VALUES ('Sport', 'Pok�j dla zainteresowanych sportem.');

-- ----------------------------
-- sprawdzenie poprawno�ci bazy
-- ----------------------------

-- odpytanie bazy o ka�d� tabel�
SELECT * FROM Users;
SELECT * FROM Rooms;